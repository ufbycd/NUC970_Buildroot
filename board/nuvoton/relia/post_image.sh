#!/bin/sh

ROOTFS_INSTALL_PATH=../nfs/rootfs
ROOTFS_IMAGE_PATH=../tftp

echo 'copy image to tftp'
cp $BINARIES_DIR/rootfs.cramfs $ROOTFS_IMAGE_PATH/rootfs.cramfs

echo 'Installing rootfs...'
sudo rm -rf $ROOTFS_INSTALL_PATH/*
sudo tar -xpf $BINARIES_DIR/rootfs.tar -C $ROOTFS_INSTALL_PATH
echo 'Installed rootfs to nfs.'
