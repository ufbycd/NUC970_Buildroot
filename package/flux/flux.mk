################################################################################
#
# flux
#
################################################################################

FLUX_VERSION = master
FLUX_SITE = https://gitee.com/ufbycd/flux.git
FLUX_SITE_METHOD = git
FLUX_LICENSE = GPLv2+
FLUX_LICENSE_FILES = LICENSE

$(eval $(autotools-package))
$(eval $(host-autotools-package))
