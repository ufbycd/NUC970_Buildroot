################################################################################
#
# directfb-examples
#
################################################################################

DIRECTFB_EXAMPLES_VERSION = master
DIRECTFB_EXAMPLES_SITE = https://gitee.com/ufbycd/DirectFB-examples.git
DIRECTFB_EXAMPLES_SITE_METHOD = git
DIRECTFB_EXAMPLES_LICENSE = MIT
DIRECTFB_EXAMPLES_LICENSE_FILES = COPYING
DIRECTFB_EXAMPLES_DEPENDENCIES = directfb jpeg libpng
DIRECTFB_EXAMPLES_CONF_OPTS += LIBS="-lpng -ljpeg"

$(eval $(autotools-package))
